﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JWTAuthExampleApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace JWTAuthExampleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        public AuthenticationController(IConfiguration config, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("You Made It");
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] LoginModel login)
        {
            var user = new IdentityUser
            {
                UserName = login.Username,
                Email = ""
            };

            var result = await _userManager.CreateAsync(user, login.Password);

            if (result.Succeeded)
            {
                return Ok("User created");
            } else
            {
                return StatusCode(StatusCodes.Status400BadRequest);
            }
        }


        [HttpPost("Authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginModel login)
        {
            var user = await _userManager.FindByNameAsync(login.Username);

            if (user != null)
            {
                //Sign in
                var signInResult = await _signInManager.PasswordSignInAsync(user, login.Password, false, false);

                if (signInResult.Succeeded)
                {
                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString())
                    };

                    var secretBytes = Encoding.UTF8.GetBytes(_config.GetValue<string>("SecretKey"));

                    var key = new SymmetricSecurityKey(secretBytes);

                    var algorithm = SecurityAlgorithms.HmacSha256;

                    var signingCredentials = new SigningCredentials(key, algorithm);

                    var token = new JwtSecurityToken(
                        _config.GetValue<string>("Issuer"),
                        _config.GetValue<string>("Audiance"),
                        claims,
                        notBefore: DateTime.Now,
                        expires: DateTime.Now.AddHours(1),
                        signingCredentials
                        );

                    var tokenJson = new JwtSecurityTokenHandler().WriteToken(token);

                    return Ok(new { access_token = tokenJson });
                }
                else
                {
                    return StatusCode(StatusCodes.Status401Unauthorized, "Invalid Credentials");
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status401Unauthorized, "Invalid Credentials");
            }
        }

        [HttpPost("Logout")]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();

            return Ok();
        }

        [Authorize]
        [HttpGet("Secret")]
        public IActionResult Secret()
        {
            return Ok("You got to the secret");
        }
    }
}
